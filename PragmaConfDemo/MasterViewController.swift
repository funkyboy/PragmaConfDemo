//
//  MasterViewController.swift
//  PragmaConfDemo
//
//  Created by Cesare Rocchi on 11/10/16.
//  Copyright © 2016 Cesare Rocchi. All rights reserved.
//

import UIKit
import WebKit

let MESSAGE_HANDLER = "didFetchSpeakers"

class MasterViewController: UITableViewController, WKScriptMessageHandler {
  
  var detailViewController: DetailViewController? = nil
  var speakers = [Speaker]()
  var speakersWebView:WKWebView?
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    title = "Pragma Conference Speakers"
    
    let speakersWebViewConfiguration = WKWebViewConfiguration()
    let scriptURL = Bundle.main.path(forResource: "fetchSpeakers", ofType: "js")
    
    let jsScript = try? String(contentsOfFile:scriptURL!,
                               encoding:String.Encoding.utf8)
    
    if let jsScript = jsScript {
      let fetchAuthorsScript = WKUserScript(source: jsScript,
                                            injectionTime: .atDocumentEnd,
                                            forMainFrameOnly: true)
      speakersWebViewConfiguration.userContentController.addUserScript(fetchAuthorsScript)
      speakersWebViewConfiguration.userContentController.add(self, name: MESSAGE_HANDLER)
      
      speakersWebView = WKWebView(frame: CGRect.zero, configuration:speakersWebViewConfiguration)
      
      if let speakersWebView = speakersWebView {
        speakersWebView.addObserver(self, forKeyPath: "loading", options: .new,context: nil)
        speakersWebView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
        let speakersURL = URL(string:"http://www.pragmaconference.com/#speakers");
        let speakersRequest = URLRequest(url:speakersURL!)
        speakersWebView.load(speakersRequest)
      }
    }
  }
  
  func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
    if (message.name == MESSAGE_HANDLER) {
      if let resultArray = message.body as? [Dictionary<String, String>] {
        for d in resultArray {
          let speaker = Speaker(dictionary: d as NSDictionary)
          speakers.append(speaker);
        }
      }
      tableView.reloadData()
    }
  }
  
  // MARK: - KVO
  
  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    if let key = keyPath {
      switch key {
      case "loading":
        UIApplication.shared.isNetworkActivityIndicatorVisible = speakersWebView!.isLoading
        
      case "estimatedProgress":
        print("progress = \(speakersWebView!.estimatedProgress)")
        
      default:
        print("unknown key")
      }
    }
  }
  
  
  override func viewWillAppear(_ animated: Bool) {
    self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
    super.viewWillAppear(animated)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - Segues
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "showDetail" {
      // This method is intentionally left blank
    }
  }
  
  // MARK: - Table View
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return speakers.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
    
    cell.tag = indexPath.row;
    let speaker = speakers[indexPath.row]
    cell.textLabel?.text = speaker.speakerName
    cell.imageView?.image = nil
    
    // Code written in a hurry :)
    DispatchQueue.global(qos: .default).async {
      if let url = URL(string: speaker.avatarURL) {
        let imageData = try? Data(contentsOf: url)
        if let imageData = imageData {
          DispatchQueue.main.async(execute: {
            if (cell.tag == indexPath.row) {
              cell.imageView?.image = UIImage(data:imageData)
              cell.setNeedsLayout()
            }
          })
        }
      } else {
        //print("I am sorry Krzysztof :( url is \(speaker.avatarURL)")
      }
    }
    return cell
  }
  
}

