//
//  Speaker.swift
//  PragmaConfDemo
//
//  Created by Cesare Rocchi on 11/10/16.
//  Copyright © 2016 Cesare Rocchi. All rights reserved.
//

import UIKit

class Speaker: NSObject {
  var speakerName: String = ""
  var avatarURL: String = ""
  
  init(dictionary: NSDictionary) {
    self.speakerName = dictionary["speakerName"] as! String
    let s = dictionary["avatarURL"] as! String
    self.avatarURL = "http://www.pragmaconference.com/" + s
    super.init()
  }
}
