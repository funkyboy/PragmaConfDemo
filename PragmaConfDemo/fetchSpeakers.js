var speakerTags = document.querySelectorAll('.speaker')

speakers = []

for (var i = 0; i < speakerTags.length; i++) {
  var speaker = speakerTags[i]
  var avatarURL = speaker.querySelector('.img-responsive').getAttribute('src')
  var name = speaker.querySelector('h3.name').textContent
  speakers.push({"speakerName" : name, "avatarURL" : avatarURL})
}

webkit.messageHandlers.didFetchSpeakers.postMessage(speakers)
